package adivina;

import static adivina.PanelOperacion.*;
import javax.swing.JOptionPane;
public class HiloSegundero extends Thread{
    private int segundo = 0;
    private boolean isRunning = false;
   
    @Override
    public void run(){
        isRunning = true;
        while(isRunning){
            try {
                Thread.sleep(1000);
                segundo = (segundo + 1) % 5;
            } catch (InterruptedException ex) {

            }
            if(segundo == 0){
                JOptionPane.showMessageDialog(null, "Haz tardado mucho, haz perdido una vida\nVida Restantes: " + (PanelOperacion.vidas - 1));
                PanelOperacion.vidas--;
                if(vidas == 0){
                    JOptionPane.showMessageDialog(null, "Haz perdido, ya no te quedan vidas.\nTotal de Aciertos: " + aciertos);
                    num1.setText("");
                    num2.setText("");
                    ope.setText("");
                    igual.setText("");
                    txtResultado.setVisible(false);
                    btnValidar.setVisible(false);
                    isRunning = false;
                    return;
                }
            }
        }
    }
    
    public void destruir(){
        isRunning = false;
    }
}
