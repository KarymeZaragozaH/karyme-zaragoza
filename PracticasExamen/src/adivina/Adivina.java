package adivina;

import javax.swing.JFrame;

public class Adivina {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Adivina");
        PanelOperacion panel = new PanelOperacion();
        frame.add(panel);
        frame.setSize(350, 250);
        frame.setLocation(100, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
}
