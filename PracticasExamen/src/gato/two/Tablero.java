package gato.two;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

class Tablero extends WindowAdapter{
    public void win(){
        String message = "Felicidades, Ganaste!";
        JOptionPane pane = new JOptionPane(message);
        JDialog dialog = pane.createDialog(new JFrame(), "Ganaste");
        dialog.show();
    }
    
    public void draw(){
        String message = "Los jugadores quedaron en Empate :(";
        
        JOptionPane pane = new JOptionPane(message);
        JDialog dialog = pane.createDialog(new JFrame(), "Empate");
        dialog.show();
    }
    
    @Override
    public void windowClosing(WindowEvent we){
        System.exit(0);
    }
}