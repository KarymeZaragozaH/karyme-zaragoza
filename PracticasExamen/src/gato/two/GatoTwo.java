package gato.two;

import java.applet.Applet;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.PrintStream;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GatoTwo extends Applet
implements MouseListener, ActionListener{
    private JFrame frame;
    private int bandera = 2;
    private int n;
    private int m;
    private int i = 0;
    public static int bug = 0;
    private char[] ch = new char[9];
    private JButton btnLimpiar;
    private JButton btnSalir;
    private String s1 = "";
    
    public GatoTwo(){
        frame = new JFrame("Gato");
        btnLimpiar = new JButton("Limpiar");
        btnSalir = new JButton("Salir");
        frame.add(this.btnLimpiar);
        frame.add(this.btnSalir);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        btnLimpiar.setBounds(650, 50, 90, 60);
        btnSalir.setBounds(650, 250, 90, 60);
        frame.addMouseListener(this);
        for (i = 0; i < 9; i++)
        ch[i] = 'B';
        btnLimpiar.addActionListener(this);
        btnSalir.addActionListener(this);
        String message = "Para Iniciar un Juego, escoja la casilla que desee.";
        
        JOptionPane pane = new JOptionPane(message);
        JDialog dialog = pane.createDialog(new JFrame(), "Gato");
        dialog.show();
        Graphics g = this.frame.getGraphics();
        g.drawLine(200, 0, 200, 600);
        g.drawLine(400, 0, 400, 600);
        g.drawLine(0, 200, 600, 200);
        g.drawLine(0, 400, 600, 400);
        g.drawLine(600, 0, 600, 600);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent ae){
        if (ae.getSource() == this.btnLimpiar){
            this.frame.setVisible(false);
            bug = 0;
            new GatoTwo();
        }
        if (ae.getSource() == this.btnSalir){
            System.exit(0);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) { Graphics2D g2;
        Graphics g = this.frame.getGraphics();
        g.drawLine(200, 0, 200, 600);
        g.drawLine(400, 0, 400, 600);
        g.drawLine(0, 200, 600, 200);
        g.drawLine(0, 400, 600, 400);
        g.drawLine(600, 0, 600, 600);
        this.bandera -= 1;
        int x = e.getX();
        int y = e.getY();
        if (this.bandera == 1)
        {
            if ((x < 200) && (y < 200)) { this.m = 0; this.n = 0; this.ch[0] = 'R'; }
            if ((x > 200) && (x < 400) && (y < 200)) { this.m = 200; this.n = 0; this.ch[1] = 'R'; }
            if ((x > 400) && (x < 600) && (y < 200)) { this.m = 400; this.n = 0; this.ch[2] = 'R'; }
            if ((x < 200) && (y > 200) && (y < 400)) { this.m = 0; this.n = 200; this.ch[3] = 'R'; }
            if ((x > 200) && (x < 400) && (y > 200) && (y < 400)) { this.m = 200; this.n = 200; this.ch[4] = 'R'; }
            if ((x > 400) && (x < 600) && (y > 200) && (y < 400)) { this.m = 400; this.n = 200; this.ch[5] = 'R'; }
            if ((x < 200) && (y > 400) && (y < 600)) { this.m = 0; this.n = 400; this.ch[6] = 'R'; }
            if ((x > 200) && (x < 400) && (y > 400) && (y < 600)) { this.m = 200; this.n = 400; this.ch[7] = 'R'; }
            if ((x > 400) && (x < 600) && (y > 400) && (y < 600)) { this.m = 400; this.n = 400; this.ch[8] = 'R'; }
            g.setColor(new Color(77, 176, 230));
            g2 = (Graphics2D)g;
            g2.setStroke(new BasicStroke(10.0F));
            g.drawOval(this.m + 10, this.n + 10, 159, 159);
        }
        
        if (this.bandera == 0)
        {
            if ((x < 200) && (y < 200)) { this.m = 0; this.n = 20; this.ch[0] = 'P'; }
            if ((x > 200) && (x < 400) && (y < 200)) { this.m = 200; this.n = 20; this.ch[1] = 'P'; }
            if ((x > 400) && (x < 600) && (y < 200)) { this.m = 400; this.n = 20; this.ch[2] = 'P'; }
            if ((x < 200) && (y > 200) && (y < 400)) { this.m = 0; this.n = 200; this.ch[3] = 'P'; }
            if ((x > 200) && (x < 400) && (y > 200) && (y < 400)) { this.m = 200; this.n = 200; this.ch[4] = 'P'; }
            if ((x > 400) && (x < 600) && (y > 200) && (y < 400)) { this.m = 400; this.n = 200; this.ch[5] = 'P'; }
            if ((x < 200) && (y > 400) && (y < 600)) { this.m = 0; this.n = 400; this.ch[6] = 'P'; }
            if ((x > 200) && (x < 400) && (y > 400) && (y < 600)) { this.m = 200; this.n = 400; this.ch[7] = 'P'; }
            if ((x > 400) && (x < 600) && (y > 400) && (y < 600)) { this.m = 400; this.n = 400; this.ch[8] = 'P'; }
            g2 = (Graphics2D)g;
            g2.setStroke(new BasicStroke(10.0F));
            g.setColor(new Color(77, 176, 230));
            g.drawLine(this.m + 10, this.n + 13, this.m + 169, this.n + 164);
            g.drawLine(this.m + 169, this.n + 10, this.m + 10, this.n + 169);
            this.bandera += 2;
        }
        
        for (this.i = 0; this.i < 3; this.i += 1)
        {
            if ((this.ch[this.i] != 'B') &&
            (this.ch[(this.i + 3)] == this.ch[this.i]) && (this.ch[(this.i + 6)] == this.ch[this.i]))
            {
                new Tablero().win();
                bug = 1;
            }
        }
        
        for (this.i = 0; this.i < 7; this.i += 1)
        {
            if (this.ch[this.i] != 'B')
            {
                if ((this.ch[this.i] == this.ch[(this.i + 1)]) && (this.ch[this.i] == this.ch[(this.i + 2)]))
                {
                    new Tablero().win();
                    bug = 1;
                }
                this.i += 2;
            }
            else {
                this.i += 2;
            }
        }
        if ((this.ch[4] != 'B') && ((
        ((this.ch[0] == this.ch[4]) && (this.ch[4] == this.ch[8])) || ((this.ch[2] == this.ch[4]) && (this.ch[4] == this.ch[6])))))
        {
            new Tablero().win();
            bug = 1;
        }
        
        for (this.i = 0; (this.i < 9) &&
        (this.ch[this.i] != 'B'); this.i += 1)
        {
            if (this.i == 8)
            {
                if (bug == 0)
                new Tablero().draw();
                bug = 0;
            }
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e){}
    
    @Override
    public void mouseEntered(MouseEvent e){}
    
    @Override
    public void mouseExited(MouseEvent e) {}
    
    @Override
    public void mousePressed(MouseEvent e) {}
}
